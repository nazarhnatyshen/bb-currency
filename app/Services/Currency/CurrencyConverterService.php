<?php

namespace App\Services\Currency;

class CurrencyConverterService
{
    /**
     * @var CurrencyFetcherService
     */
    private $currencyFetcherService;

    public function __construct(CurrencyFetcherService $currencyFetcherService)
    {
        $this->currencyFetcherService = $currencyFetcherService;
    }

    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     * @throws \Exception
     */
    public function convert(float $amount, string $from, string $to): float
    {
        $currencyList = $this->currencyFetcherService->getCurrencyList();

        if (empty($currencyList)) {
            throw new \Exception('Something went wrong with currency fetch service');
        }

        if (!isset($currencyList[$from])) {
            throw new \Exception('Invalid from');
        }

        if (!isset($currencyList[$to])) {
            throw new \Exception('Invalid to');
        }

        return (float)($currencyList[$to] / $currencyList[$from] * $amount);
    }
}