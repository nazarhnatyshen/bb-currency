<?php

namespace App\Http\Controllers;

use App\Services\Currency\CurrencyConverterService;
use App\Services\Currency\CurrencyFetcherService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends Controller
{
    public function get(CurrencyFetcherService $currencyFetcherService)
    {
        $list = $currencyFetcherService->getCurrencyList();

        return response()->json([
            'data' => [
                'currencies' => array_keys($list),
            ],
        ]);
    }

    public function convert(CurrencyConverterService $currencyConverterService, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'from' => 'required|string|size:3',
            'to' => 'required|string|size:3',
        ]);

        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first());
        }

        $amount = $request->get('amount');
        $from = $request->get('from');
        $to = $request->get('to');

        $converted = $currencyConverterService->convert($amount, strtoupper($from), strtoupper($to));

        return response()->json([
            'data' => [
                'attributes' => [
                    'amount' => $amount,
                    'converted' => $converted,
                    'from' => $from,
                    'to' => $to,
                ],
            ],
        ]);
    }
}