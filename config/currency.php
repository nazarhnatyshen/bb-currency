<?php

return [
    'base' => env('BASE_CURRENCY', 'USD'),
    'currency-list-url' => 'https://openexchangerates.org/api/latest.json?app_id=' . env('OER_APP_ID') . '&base=' . env('BASE_CURRENCY'),
    'cache-duration' => env('CACHE_DURATION', 1),
];