<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('currency', [
    'as' => 'currency.list',
    'uses' => 'CurrencyController@get'
]);
Route::get('convert-currency', [
    'as' => 'currency.convert',
    'uses' => 'CurrencyController@convert',
]);