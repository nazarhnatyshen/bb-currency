<?php


class GetCurrencyConvertationCest
{
    public function testConvertation(ApiTester $I)
    {
        $I->wantToTest('Currencies converation');
        $I->sendGET(route('currency.convert'), [
            'amount' => 1,
            'from' => 'USD',
            'to' => 'USD',
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->canSeeResponseContainsJson([
            'data' => [
                'attributes' => [
                    'amount' => 1,
                    'converted' => 1,
                ],
            ],
        ]);
    }
}
