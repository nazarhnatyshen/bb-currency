<?php


class GetCurrenciesListCest
{
    public function getCurrencyListTest(ApiTester $I)
    {
        $I->wantToTest('Get currencies list');
        $I->sendGET(route('currency.list'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('data.currencies');
    }
}
