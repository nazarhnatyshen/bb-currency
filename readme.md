**How to run?**

1. `git clone git@bitbucket.org:nazarhnatyshen/bb-currency.git`
2. `cp .env.example .env`
3. `docker-compose build && docker-compose up -d`
4. `docker-compose exec -T php /bin/bash -l -c 'composer install'`
4. Add `127.0.0.1   demo.dev` to your /private/etc/hosts file