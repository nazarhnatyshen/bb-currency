<?php

namespace App\Services\Currency;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class CurrencyFetcherService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCurrencyList(): array
    {
        $cacheKey = 'currency-list';

        $currencies = Cache::get($cacheKey);
        if (empty($currencies)) {
            $currencies = $this->fetchCurrencyListFromApi();

            Cache::put($cacheKey, $currencies, config('currency.cache-duration'));
        }

        return $currencies;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function fetchCurrencyListFromApi(): array
    {
        $result = $this->client->get(config('currency.currency-list-url'));
        if ($result->getStatusCode() != 200) {
            throw new \Exception('Invalid http response');
        }

        $contentType = $result->getHeader('content-type');
        if (empty($contentType[0]) || strpos($contentType[0], 'application/json') === FALSE) {
            throw new \Exception('Invalid response from currency list api');
        }

        $response = json_decode($result->getBody()->getContents(), true);
        if (isset($response['rates'])) {
            return $response['rates'];
        }

        return null;
    }
}