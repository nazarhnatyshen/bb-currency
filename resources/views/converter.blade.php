<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>converter</title>
    </head>
    <body>
        <div class="container">
            <form id="converter" action="{{ route('currency.convert') }}" method="get">
                <select name="from" id="from" required></select>
                <input id="from-amount" name="amount" type="number" placeholder="Enter amount" required>
                <input id="to-amount" name="result" type="number" placeholder="Enter amount" disabled>
                <select name="to" id="to" required>
                </select>
                <button type="submit">CONVERT</button>
            </form>
        </div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: '{{ route('currency.list') }}',
                type: 'get',
                dataType: 'json',
                success: function (result) {
                    fillCurrenciesList($('#from'), result.data.currencies);
                    fillCurrenciesList($('#to'), result.data.currencies);
                }
            });

            $('#converter').submit(function (e) {
                e.preventDefault();
                let amount = $('#from-amount').val();
                let from = $('#from').val();
                let to = $('#to').val();

                convertCurrency(amount, from, to);
            });
        });

        function convertCurrency(amount, from, to) {
            $.ajax({
                url: '{{ route('currency.convert') }}',
                type: 'get',
                dataType: 'json',
                data: $('#converter').serialize(),
                success: function (result) {
                    let converted = result.data.attributes.converted;
                    $('#to-amount').val(converted);
                    appendConvertLog(amount, converted, from, to);
                }
            });
        }

        function appendConvertLog(amount, converted, from, to) {
            let el = $('<p>').text(amount + ' ' + from + ' -> ' + converted + ' ' + to);
            $('.container').append(el);
        }

        function fillCurrenciesList(element, currencies) {
            $.each(currencies, function () {
                let newOption = $('<option>').text(this).attr('value', this);
                if (this == '{{ config('currency.base') }}') {
                    newOption = newOption.attr('selected', '');
                }
                element.append(newOption);
            });
        }
    </script>
    <style>
        body {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            overflow: auto;
        }
        .container {
            width: 500px;
            height: 400px;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
        }
    </style>
</html>